﻿#include <ctime>
#include <iostream>

int main()
{
    int sum = 0;
     const int N = 4;
    int array1[N][N] = { {0}, {0} };
    std::cout << "Showing our array, where the element array[i][j] = i + j: \n";
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < N; j++)
        {
           array1[i][j] = { i + j };

            std::cout << array1[i][j];
        }
        std::cout << '\n';
    }
    int i = 0;

    time_t now = time(0);
    struct tm timeinfo;
    localtime_s(&timeinfo, &now);
    int iday = timeinfo.tm_mday;

    int k = iday % N;

    for (int i = k; i <= k; i++)
    {
        for (int j = 0; j < N; j++)
        {
            sum = sum + array1[i][j];
        }
       
    }
   
    std::cout << "The sum of elements of array with [i] index = the current day of month mod N, (where N is the size of one dimension of array):  " << sum;
    return 0;
 }